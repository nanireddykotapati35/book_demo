// importing express from the express module
import express from "express";
//importign bodyparser from the body-parser module
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

//importing cors
import cors from "cors";
//enable the express server to respond to preflight requests
app.use(
  cors({
    origin: "*",
  })
);

import post_books_data_handler from "./path_handlers/post_apis.js";

import {get_books_details_handler,get_latest_details_handler,get_old_details_handler,get_books_specific_handler, get_users_by_id} from "./path_handlers/get_apis.js";

import books_param_count_checker from "./middlewares/book_validation.js";

import update_path_handler from "./path_handlers/update_apis.js";

import delete_product from "./path_handlers/delete_apis.js";

app.post("/register",post_books_data_handler);

app.get("/getdata",get_books_details_handler);

app.get("/getuser/:id",get_users_by_id);

app.get("/oldest",get_old_details_handler);

app.get("/latest",get_latest_details_handler);

app.patch("/updateuser/:id",update_path_handler);

app.delete("/deleteuser/:id",delete_product);



app.listen(2000, () => {
  console.log("http://localhost:2000");
});

