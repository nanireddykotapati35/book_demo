import { Button } from "@mui/material";
import React from "react";
import "./bookHome.css";
import { Link } from "react-router-dom";

const bookHome = () => {
  return (
    <>
    <div className="cont">
      <div className="h6">
    <h6 style={{fontSize:"20px"}}>"some books you read.
      some books you enjoy.
      But some books swallow you up
      Heart and Soul."
    </h6>
    <span style={{fontSize:"12px"}}>JOANNA HARRIS</span>

    <div className="con d-flex pt-3">
          
    <Button style={{backgroundColor:"#86B6C9",color:"white"}} href="/">ViewBooks</Button>
          
        </div>
    </div>
      <div className="body d-flex flex-row" style={{overflow:"auto"}}>
        
        <div className="shelf m-3">
          
          {/* Clock no marques las horas */}
          <div className="clock">
          
            <div className="knot" />
            
            <div className="hand hour-hand" />
            <div className="hand min-hand" />
            <div className="hand second-hand" />
            <div className="clock-ear-left" />
            <div className="clock-ear-right" />
            <div className="clock-foot-left" />
            <div className="clock-foot-right" />
          </div>
          
          {/* Books */}
          <div className="book-1" />
          <div className="book-2" />
          <div className="book-3" />
          {/* Cactus */}
          <div className="cactus">
            <div className="pot">
              <div className="pot-top" />
              <div className="pot-bottom" />
            </div>
            <div className="plant-left" />
            <div className="plant-right" />
          </div>
        </div>

      </div>
      </div>
    </>
  );
};

export default bookHome;
